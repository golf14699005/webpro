<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
<style>
body, html {height: 100%}
body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
.menu {display: none}
.bgimg {
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url("222.jpg");
    min-height: 90%;
}
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top w3-hide-small">
  <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
    <a href="Index1.php" class="w3-bar-item w3-button">HOME</a>
    <a href="MANU.php" class="w3-bar-item w3-button">BOOKS</a>
    <a href="product.php" class="w3-bar-item w3-button">SHOPPING BASKET</a>
    <a href="edit_profile.php" class="w3-bar-item w3-button">EDIT PROFILE</a>
    <a href="Index.php" class="w3-bar-item w3-button">LOGOUT</a>

  </div>
</div>
<div class="w3-container w3-padding-64 w3-blue-grey w3-grayscale-min w3-xlarge">
  <div class="w3-content">
    <h1 class="w3-center w3-jumbo1" style="margin-bottom:64px">EDIT PROFILE</h1> 

    <form name="form1" method="post" action="">

      <p><input class="w3-input w3-padding-16 w3-border" type="text"  required name="name"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="text"  required name="lname"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="text"  required name="phone"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="text"  required name="add"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="text"  required name="email"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="text"  required name="user"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="text"  required name="pass"></p>
     
    
     <input class="w3-input w3-padding-16 w3-border" onclick="myFunction()" type="submit" name="Submit" value="OK">

    </form>
     
      <script>
    function myFunction() {
    alert("Edited");
    }
     </script>

  </div>
</div>
</body>
</html>
